@extends('layouts.app-archiveBonus')

@section('content')
  @include('post-types.bonus.archive.title')
  @include('post-types.bonus.archive.filtersTags')

  <section class="container bonusArchive__mainContainer">
    @include('post-types.bonus.archive.mainLoop')
    @include('post-types.bonus.archive.sidebar')
  </section>
@endsection
