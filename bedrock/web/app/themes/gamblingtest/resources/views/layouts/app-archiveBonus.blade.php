<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class('bonusArchive') @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="bonusArchive__content" role="document">
      @yield('content')
    </div>
    @php wp_footer() @endphp
  </body>
</html>
