<main class="bonusArchive__main">
  @if(have_posts())
    @while(have_posts())
      {!! the_post() !!}
      @include('post-types.bonus.archive.loop')
    @endwhile
  @endif
</main>
