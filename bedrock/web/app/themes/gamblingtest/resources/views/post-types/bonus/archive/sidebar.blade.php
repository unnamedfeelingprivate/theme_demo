<aside class="bonusArchive__sidebar">
  <div class="bonusArchive__widget">
    <div class="bonusArchive__widget--title">
      <h3>Лучшие букмекеры</h3>
    </div>
    <div class="bonusArchive__widget--body">
      <div class="bonusArchive__widget--content">
        <div class="bestBookmakersList">
          @for($i=0; $i < 3; $i++)
          <article class="bestBookmakersList__item">
            <div class="bestBookmakersList__item--image">
              <img src="@asset('images/items/bookmaker.jpg')" alt="bookmaker-{{$i}}">
            </div>
            <div class="bestBookmakersList__item--infoWrapper">
              <div class="bestBookmakersList__item--title">
                <h4>Bwin {{$i}}</h4>
                <span>Бонус до 3000 RUB</span>
              </div>
              <div class="itemRating">
                <i class="fa-star-full"></i>
                <span>4.4</span>
              </div>
              <div class="bestBookmakersList__item--link">
                <a href="#" class="button button__colored green">Сделать ставку</a>
              </div>
            </div>
          </article>
          @endfor
        </div>
      </div>
      <div class="bonusArchive__widget--footer">
        <a href="#">Посмотреть все <i class="fa-chevron-right"></i></a>
      </div>
    </div>

  </div>

  <div class="bonusArchive__widget">
    <div class="bonusArchive__widget--title">
      <h3>Последние отзывы</h3>
    </div>
    <div class="bonusArchive__widget--body">
      <div class="bonusArchive__widget--content noborder">
        <div class="rewiewsList">
          @for($i=0; $i < 2; $i++)
            <article class="rewiewsList__item">
              <div class="rewiewsList__item--commenterName">
                <span>William Hill</span>
              </div>
              <div class="itemRating">
                <i class="fa-star-full"></i>
                <span>4.4</span>
              </div>
              <div class="rewiewsList__item--commentText">
                <p>Играю со смарта все очень удобно без вылетов и зависаний, вывод средств можно сказать
                  мгновенный...</p>
                <a href="#">Читать далее</a>
              </div>
              <div class="rewiewsList__item--commentMeta">
                <i class="fa-user"></i>
                <span>Сергей Аржаков</span>
              </div>
              <div class="rewiewsList__item--commentMeta">
                <i class="fa-clock-light"></i>
                <span>1 сентября 2019</span>
              </div>
            </article>
          @endfor
        </div>
      </div>
      <div class="bonusArchive__widget--footer">
        <a href="#">Посмотреть все <i class="fa-chevron-right"></i></a>
      </div>
    </div>
  </div>
</aside>
