<article {{post_class('bonusArchive__article')}}>
  <div class="bonusArchive__article--largeImage">
    <img src="@asset('images/items/img.png')" alt="{{$post->post_title}}">
    <div class="bonusArchive__article--largeImageLabels">
      <ul class="bonusArchive__article--largeImageLabels">
        <li><span>Букмекер месяца</span></li>
        <li><span>Щедрые бонусы</span></li>
      </ul>
    </div>

  </div>

  <div class="bonusArchive__article--infoWrapperBlock">
    <div class="bonusArchive__article--titleBlock">
      <ul class="bonusArchive__article--titleBlockLabels">
        <li class="bonusArchive__article--titleBlockLabels-item">Приветственный</li>
      </ul>

      <div class="bonusArchive__article--smallImage">
        <img src="@asset('images/items/img.png')" alt="{{$post->post_title}}">
      </div>

      <h3>{{$post->post_title}}</h3>
    </div>

    <div class="bonusArchive__article--informationBlock">
      <ul class="bonusArchive__article--informationBlockList">
        <li class="bonusArchive__article--informationBlockListItem">
          <span class="bonusArchive__article--informationBlockListItem-title">Макс. сумма:</span>
          <span class="bonusArchive__article--informationBlockListItem-value">100 000 RUB</span>
        </li>
        <li class="bonusArchive__article--informationBlockListItem">
          <span class="bonusArchive__article--informationBlockListItem-title">Быстрые выплаты:</span>
          <span class="bonusArchive__article--informationBlockListItem-value">нет</span>
        </li>
        <li class="bonusArchive__article--informationBlockListItem">
          <span class="bonusArchive__article--informationBlockListItem-title">Действителен:</span>
          <span class="bonusArchive__article--informationBlockListItem-value">до 31 ноября</span>
        </li>
        <li class="bonusArchive__article--informationBlockListItem">
          <span class="bonusArchive__article--informationBlockListItem-title">Мин. депозит:</span>
          <span class="bonusArchive__article--informationBlockListItem-value">100 RUB</span>
        </li>
        <li class="bonusArchive__article--informationBlockListItem">
          <span class="bonusArchive__article--informationBlockListItem-title">Кэшбек:</span>
          <span class="bonusArchive__article--informationBlockListItem-value">50%</span>
        </li>
        <li class="bonusArchive__article--informationBlockListItem">
          <span class="bonusArchive__article--informationBlockListItem-title">Бонус код:</span>
          <span class="bonusArchive__article--informationBlockListItem-value">нет</span>
        </li>
      </ul>

      <ul class="bonusArchive__article--informationBlockButtons">
        <li><a href="#" class="button button__colored blue">1xСтавка</a></li>
        <li><a href="#" class="button blueBorder">Все бонусы (999)</a></li>
        <li><a href="#" class="button button__colored red">Получить</a></li>
      </ul>
    </div>
  </div>

  <div class="bonusArchive__article--accordeon js-collapsible">
    <a href="#" class="bonusArchive__article--accordeon-trigger js-collapsibleTrigger">Условия получения</a>
    <div class="bonusArchive__article--accordeon-content js-collapsibleContent hidden">
      <ol>
        <li>Бонусы активируются в личном кабинете в течение 3 дней.</li>
        <li>Срок отыгрыша денежной части бонуса 14 дней.</li>
        <li>Максимально допустимый выигрыш с бонуса для вывода 3 750 RUB.</li>
        <li>Ставка во время отыгрыша - не более 350 RUB.</li>
      </ol>
    </div>
  </div>

</article>
