let mobileMenuComponent = new function () {
  let self = this

  self.init = () => {
    self.buttons = document.querySelectorAll('.js-mobileMenuBtn')
    self.menu = document.querySelector('.js-mobileMenu')

    if(self.buttons) self.buttons.forEach(button => self.handleButton(button))

    window.addEventListener('resize', self.resizeHandler)
  }

  self.handleButton = button => {
    button.addEventListener('click', event => {
      event.preventDefault()

      button.classList.toggle('is-active')

      if(!self.menu) return false;

      if(!self.menu.classList.contains('active')){
        self.openMenu()
      } else {
        self.closeMenu()
      }
    })
  }

  self.openMenu = () => {
    self.menu.style.height = self.menu.scrollHeight + 'px';
    self.menu.classList.add('active')
  }

  self.closeMenu = () => {
    self.menu.style.height = '0px';

    setTimeout(() => {
      self.menu.classList.remove('active')
      self.menu.style.height = ''
    }, 500)
  }

  self.resizeHandler = () => {
    console.log()
    if(document.body.clientWidth < 1200) {
      if(self.buttons) self.buttons.forEach(
        button => button.classList.remove('is-active')
      )

      if(self.menu) {
        self.menu.classList.remove('active')
        self.menu.removeAttribute('style')
      }
    }

  }
}

export default mobileMenuComponent
