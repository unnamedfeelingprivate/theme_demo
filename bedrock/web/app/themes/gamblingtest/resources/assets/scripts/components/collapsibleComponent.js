let collapsibleComponent = new function () {
  let self = this

  self.init = () => {
    self.elems = document.querySelectorAll('.js-collapsible')

    self.elemGroups = {}
    self.sortElems()

    if(self.elems) {
      self.elems.forEach(function(elem){
        let triggerButton = elem.querySelector('.js-collapsibleTrigger')

        if (triggerButton) triggerButton.addEventListener('click', self.handleToggle);

        if (elem.dataset.activeonload) self.openContent(elem.querySelector('.js-collapsibleContent'), triggerButton)
      })
    }
  }

  self.handleToggle = event => {
    event.preventDefault()

    let trg = event.target
    let content = trg.closest('.js-collapsible').querySelector('.js-collapsibleContent');

    if (self.isInGroup(content.parentNode)) self.closeOther(content.parentNode)

    if (!content.classList.contains('hidden')) {
      if (content.parentNode.dataset.allowclose && content.parentNode.dataset.allowclose === 'false') return false
      self.closeContent(content, trg)
    } else {
      self.openContent(content, trg)
    }
  }

  self.isInGroup = elem => {
    if(!elem.dataset.accordiongroup) return false;

    let group = self.elemGroups[elem.dataset.accordiongroup]
    return group && group.length
  }

  self.openContent = (content, trigger) => {
    trigger.classList.toggle('active');
    content.parentNode.classList.add('is-open')
    content.classList.remove('hidden');
    content.style.height = content.scrollHeight + 'px';
    if (trigger.dataset.showntext) trigger.innerText = trigger.dataset.showntext
  }

  self.closeContent = (content, trigger) => {
    trigger.classList.toggle('active');
    content.parentNode.classList.remove('is-open')
    content.style.height = '0px';
    if (trigger.dataset.hiddentext) trigger.innerText = trigger.dataset.hiddentext

    setTimeout(() => {
      content.classList.add('hidden')
      content.style.height = ''
    }, 500)
  }

  self.closeOther = elem => {
    let groupId = elem.dataset.accordiongroup
    if(!groupId) return false;

    if(self.elemGroups[groupId]){
      self.elemGroups[groupId].map(groupElem => {
        if(elem.isEqualNode(groupElem)) return false;

        if (groupElem.classList.contains('is-open')) {
          self.closeContent(
            groupElem.querySelector('.collapsibleContent'),
            groupElem.querySelector('.js-collapsibleTrigger')
          )
        }
      })
    }
  }

  self.sortElems = () => {
    if(self.elems){
      Array.from(self.elems).map(elem => {
        let groupId = elem.dataset.accordiongroup
        if (groupId && !self.elemGroups[groupId]) self.elemGroups[groupId] = []

        if (groupId) self.elemGroups[groupId].push(elem)
      })
    }
  }
};

// <div class="js-collapsible collapsible">
//   <div class="collapsibleContent hidden" data-height="180px" style="height: 180px;">
//     <p>Cielo Waste Solutions Corp. (“Cielo”) strategic intent is to “Lead the waste to fuels industry using environmentally friendly, economically sustainable technology”. Cielo plans to construct renewable diesel facilities using alternate waste feedstocks. World energy needs continue to rise, creating a need to reduce our demand on a declining supply of fossil fuels. Mandates from Federal, Provincial and World governments for greener energy supplies make this the right time for Cielo to commercialize their technology and scale-up the production of renewable fuels that can be blended with conventional diesel fuel and jet/marine fuel.</p>
//   </div>
//   <a href="#" class="collapsibleTrigger js-collapsibleTrigger" data-showntext="Collapse" data-hiddentext="Read more">Read more</a>
// </div>

export default collapsibleComponent
