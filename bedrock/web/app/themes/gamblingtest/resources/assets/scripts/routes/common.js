import collapsibleComponent from '../components/collapsibleComponent'
import mobileMenuComponent from '../components/mobileMenuComponent'

export default {
  init() {
    window.addEventListener('load', () => {
      collapsibleComponent.init()
      mobileMenuComponent.init()
    })

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
