import Choices from 'choices.js'

let choiceComponent = new function () {
  let self = this

  self.init = () => {
    self.elements = document.querySelectorAll('.js-choiceSelect')

    console.log(self.elements)

    if(self.elements){
      self.elements.forEach(select => {
        let config = {itemSelectText: null}

        if (select.dataset && select.dataset.initialtext) {
          config.itemSelectText = select.dataset.initialtext
        }

        if(select.multiple){
          let parent = select.parentNode,
            textarea = document.createElement('textarea')

          textarea.classList.add('hidden')
          textarea.name = select.name

          if(select.querySelectorAll('option[selected]')){
            let fieldVal = []
            select.querySelectorAll('option[selected]').forEach(option => {fieldVal.push(parseInt(option.value))})

            textarea.value = JSON.stringify(fieldVal)
          }

          parent.appendChild(textarea)

          select.name = '__' + select.name

          select.addEventListener('change', event => {
            let val = []
            if (event.target.querySelectorAll('option')){
              event.target.querySelectorAll('option').forEach(option => {val.push(parseInt(option.value))})
            }

            textarea.value = JSON.stringify(val)
          })
        }

        new Choices(select, config)
      })
    }
  }

}

export default choiceComponent
