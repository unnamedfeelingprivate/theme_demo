import choiceComponent from '../components/choiceComponent'

export default {
  init() {
    // JavaScript to be fired on all
    window.addEventListener('load', () => {
      choiceComponent.init()
    })

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
