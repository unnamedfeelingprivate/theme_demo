<?php


namespace App\Gamblingtest;


use App\Gamblingtest\Features\MetaFields;
use App\Gamblingtest\Features\PostTypes;
use App\Gamblingtest\Features\Taxonomies;

class AppHandler
{
    public function __construct()
    {
        $postTypes = new PostTypes();
        $postTypes->run();

        $taxonomies = new Taxonomies();
        $taxonomies->run();

        $metaFields = new MetaFields();
        add_action('add_meta_boxes', [$metaFields, 'run']);
        add_action('save_post', [$metaFields, 'onSavePost'], 10, 3);
//        $metaFields->run();
    }
}
