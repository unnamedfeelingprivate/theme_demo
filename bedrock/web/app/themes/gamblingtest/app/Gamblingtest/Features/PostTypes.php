<?php


namespace App\Gamblingtest\Features;


class PostTypes
{
    /**
     * @var array[]
     */
    private $config;

    public function __construct()
    {
        $this->config = [
            'review' => [
                'labels'                  => [
                    'name'                    => __('Reviews'), // Основное название типа записи
                    'singular_name'           => __('Review'), // отдельное название записи типа Book
                    'add_new'                 => __('Add'),
                    'add_new_item'            => __('Add new review'),
                    'edit_item'               => __('Edit review'),
                    'new_item'                => __('New review'),
                    'view_item'               => __('New'),
                    'search_items'            => __('Search reviews'),
                    'not_found'               => __('No reviews found'),
                    'not_found_in_trash'      => __('No reviews found in trash'),
                    'parent_item_colon'       => '',
                    'menu_name'               => __('Reviews'),
                ],
                'public'                  => true,
                'publicly_queryable'      => true,
                'show_ui'                 => true,
                'show_in_menu'            => true,
                'query_var'               => true,
                'rewrite'                 => true,
                'has_archive'             => true,
                'show_in_rest'            => true,
                'rest_base'               => '',
                'rest_controller_class'   => 'WP_REST_Posts_Controller',
                'capability_type'         => 'post',
                'hierarchical'            => false,
                'menu_position'           => null,
                'supports'                => ['title','editor','author','thumbnail','excerpt','comments','custom-fields']
            ],
            'bonus' => [
                'labels'                  => [
                    'name'                    => __('Bonuses'), // Основное название типа записи
                    'singular_name'           => __('Bonus'), // отдельное название записи типа Book
                    'add_new'                 => __('Add'),
                    'add_new_item'            => __('Add new bonus'),
                    'edit_item'               => __('Edit bonus'),
                    'new_item'                => __('New bonus'),
                    'view_item'               => __('New'),
                    'search_items'            => __('Search bonuses'),
                    'not_found'               => __('No bonuses found'),
                    'not_found_in_trash'      => __('No bonuses found in trash'),
                    'parent_item_colon'       => '',
                    'menu_name'               => __('Bonuses'),
                ],
                'public'                  => true,
                'publicly_queryable'      => true,
                'show_ui'                 => true,
                'show_in_menu'            => true,
                'query_var'               => true,
                'rewrite'                 => true,
                'has_archive'             => true,
                'show_in_rest'            => true,
                'rest_base'               => '',
                'rest_controller_class'   => 'WP_REST_Posts_Controller',
                'capability_type'         => 'post',
                'hierarchical'            => false,
                'menu_position'           => null,
                'supports'                => ['title','editor','author','thumbnail','excerpt','comments','custom-fields']
            ],
        ];
    }

    public function run()
    {
        foreach ($this->config as $key => $conf){
            register_post_type($key, $conf);
        }
    }
}
