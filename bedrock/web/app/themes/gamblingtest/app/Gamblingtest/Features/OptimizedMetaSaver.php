<?php


namespace App\Gamblingtest\Features;


use stdClass;

class OptimizedMetaSaver
{
    const PREFIX = 'GamblingtestAllMeta_';

    public static function all($post_id = '')
    {
        if ( !$post_id ) {
            $post_id = get_the_id();
        }

        $newKey = self::PREFIX . "$post_id";
        $re = get_option($newKey, json_encode([]));
        return json_decode($re);
    }

    public static function get($key = '', $post_id = '')
    {
        if ( !$post_id ) {
            $post_id = get_the_id();
        }

        if($post_id === 'option') {
            $post_id = 'options';
        }

        $newKey = self::PREFIX . "$post_id";
        $re = get_option($newKey, []);

        $reArray = json_decode($re, true);
        if (! empty($reArray) ) {
            if (isset($reArray[$key])) {
                return $reArray[$key];
            }
        }
        return [];
    }

    public static function updateFieldCache( int $post_id ) {
        $pmeta = get_post_meta($post_id);

        $meta = new stdClass;
        foreach( (array) $pmeta as $k => $v ) $meta->$k = $v[0];

        self::setAllObjects($post_id, $meta);
    }

    public static function setAllObjects($post_id, $value)
    {
        $jsonValue = json_encode($value);
        $newKey = self::PREFIX . "$post_id";
        if (self::existAllObjects($newKey)) {
            update_option($newKey, $jsonValue, 'no');
        } else {
            add_option($newKey, $jsonValue, '', 'no');
        }
    }

    public static function existAllObjects($key)
    {
        $re = get_option($key, []);

        return !empty($re);
    }
}
