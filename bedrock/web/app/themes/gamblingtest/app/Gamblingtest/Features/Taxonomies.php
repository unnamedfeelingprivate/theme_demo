<?php


namespace App\Gamblingtest\Features;


class Taxonomies
{
    private $config;

    public function __construct()
    {
        $this->config = [
            'review' => [
                'language' => [
                    'label' => __('Language'),
                    'description' => '',
                    'hierarchical' => false,
                    'public' => true,
                    'publicly_queryable' => true,
                    'update_count_callback' => '',
                    'sort' => true,
                    'labels' => array(),
                    'show_ui' => true,
                    'show_in_menu' => true,
                    'show_in_nav_menus' => true,
                    'show_tagcloud' => true,
                    'show_in_quick_edit' => true,
                    'show_admin_column' => true,
                    'rewrite' => true,
                    'show_in_rest' => false,
                    'rest_base' => '',
                    'rest_controller_class' => 'WP_REST_Terms_Controller',
                    'capabilities' => [],
                    'meta_box_cb' => NULL,
                ],
                'sitever' => [
                    'label' => __('Site version'),
                    'description' => '',
                    'hierarchical' => false,
                    'public' => true,
                    'publicly_queryable' => true,
                    'update_count_callback' => '',
                    'sort' => true,
                    'labels' => array(),
                    'show_ui' => true,
                    'show_in_menu' => true,
                    'show_in_nav_menus' => true,
                    'show_tagcloud' => true,
                    'show_in_quick_edit' => true,
                    'show_admin_column' => true,
                    'rewrite' => true,
                    'show_in_rest' => false,
                    'rest_base' => '',
                    'rest_controller_class' => 'WP_REST_Terms_Controller',
                    'capabilities' => [],
                    'meta_box_cb' => NULL,
                ],
                'paymentsystem' => [
                    'label' => __('Payment system'),
                    'description' => '',
                    'hierarchical' => false,
                    'public' => true,
                    'publicly_queryable' => true,
                    'update_count_callback' => '',
                    'sort' => true,
                    'labels' => array(),
                    'show_ui' => true,
                    'show_in_menu' => true,
                    'show_in_nav_menus' => true,
                    'show_tagcloud' => true,
                    'show_in_quick_edit' => true,
                    'show_admin_column' => true,
                    'rewrite' => true,
                    'show_in_rest' => false,
                    'rest_base' => '',
                    'rest_controller_class' => 'WP_REST_Terms_Controller',
                    'capabilities' => [],
                    'meta_box_cb' => NULL,
                ],
            ],
        ];

    }

    public function run()
    {
        foreach ($this->config as $cpt => $taxConfs){
            foreach($taxConfs as $key => $taxconf){
                register_taxonomy($key, [$cpt], $taxconf);
            }
        }
    }
}
