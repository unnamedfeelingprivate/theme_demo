<?php


namespace App\Gamblingtest\Features;


use Formr;
use PHP_CodeSniffer\Standards\PSR12\Tests\Functions\ReturnTypeDeclarationUnitTest;
use WP_Post;

class MetaFields
{
    private $cpts = ['review'];
    private $form;
    private $metaFields = ['dayPayment', 'weekPayment', 'monthPayment', 'minOut', 'dayPayment_currency', 'weekPayment_currency', 'monthPayment_currency', 'minOut_currency', 'language', 'sitever', 'paymentsystem', 'sitever', 'paymentsystem', 'sitever', 'paymentsystem'];
    private $currencies;
    private $taxsArr = ['language', 'sitever', 'paymentsystem'];
    private $termsFields;

    public function __construct()
    {
        $this->form = new Formr();
        $this->currencies = [
            '' => __('Choose your option'),
            'RUB' => 'RUB',
            'UAH' => 'UAH',
            'KZT' => 'KZT',
            'MDL' => 'MDL',
            'USD' => 'USD',
            'EUR' => 'EUR',
        ];

        $this->termsFields = $this->generateTermsArr();
    }

    public function run()
    {
        foreach($this->cpts as $cpt){
            add_meta_box(
                'gamblingtest_metabox_one',           // Unique ID
                __('Custom review post type metabox'),  // Box title
                [$this, 'outputMetaboxHtml'],  // Content callback, must be of type callable
                $cpt,                   // Post type
                'normal',
                'high'
            );
        }

    }

    public function outputMetaboxHtml(WP_Post $post, array $meta)
    {
        $postMeta = OptimizedMetaSaver::all($post->ID);
        $html = '<div class="gamblingtest_metaboxComponent">';
        $html .= wp_nonce_field( 'gamblingtest_metabox_one', 'gamblingtest_metabox_one_nonce', true, false );
        $html .= '<div class="row gamblingtest_metaboxComponent__table">
        <div class="col-md-6 col-lg-4 gamblingtest_metaboxComponent__table--item">
        '.$this->form->input_number(
                'dayPayment',
                __('Day'),
                isset($postMeta->dayPayment) ? $postMeta->dayPayment : '',
                'dayPayment',
                'min="0"',
                ''
            ).$this->form->input_select(
                'dayPayment_currency',
                '',
                isset($postMeta->dayPayment_currency) ? $postMeta->dayPayment_currency : '',
                'dayPayment_currency',
                'class="js-choiceSelect"',
                '',
                isset($postMeta->dayPayment_currency) ? $postMeta->dayPayment_currency : '',
                $this->currencies,
                false
            ).'
        </div>
        <div class="col-md-6 col-lg-4 gamblingtest_metaboxComponent__table--item">
        '.$this->form->input_number(
                'weekPayment',
                __('Week'),
                isset($postMeta->weekPayment) ? $postMeta->weekPayment : '',
                'weekPayment',
                'min="0"',
                ''
            ).$this->form->input_select(
                'weekPayment_currency',
                '',
                isset($postMeta->weekPayment_currency) ? $postMeta->weekPayment_currency : '',
                'weekPayment_currency',
                'class="js-choiceSelect"',
                '',
                isset($postMeta->weekPayment_currency) ? $postMeta->weekPayment_currency : '',
                $this->currencies,
                false
            ).'
        </div>
        <div class="col-md-6 col-lg-4 gamblingtest_metaboxComponent__table--item">
        '.$this->form->input_number(
                'monthPayment',
                __('Month'),
                isset($postMeta->monthPayment) ? $postMeta->monthPayment : '',
                'monthPayment',
                'min="0"',
                ''
            ).$this->form->input_select(
                'monthPayment_currency',
                '',
                isset($postMeta->monthPayment_currency) ? $postMeta->monthPayment_currency : '',
                'monthPayment_currency',
                'class="js-choiceSelect"',
                '',
                isset($postMeta->monthPayment_currency) ? $postMeta->monthPayment_currency : '',
                $this->currencies,
                false
            ).'
        </div>
        <div class="col-6 gamblingtest_metaboxComponent__table--item">
        '.$this->form->input_number(
                'minOut',
                __('Minimal withdrawal'),
                isset($postMeta->minOut) ? $postMeta->minOut : '',
                'minOut',
                'min="0"',
                ''
            ).$this->form->input_select(
                'minOut_currency',
                '',
                isset($postMeta->minOut_currency) ? $postMeta->minOut_currency : '',
                'minOut_currency',
                'class="js-choiceSelect"',
                '',
                isset($postMeta->minOut_currency) ? $postMeta->minOut_currency : '',
                $this->currencies,
                false
            ).'
        </div>
        </div>';

        $languagesOpts = [];
        if(!empty($this->termsFields['language'])){
            foreach ($this->termsFields['language'] as $term) {
                $languagesOpts[$term->term_id] = $term->name;
            }
        }

        $html .= '<div class="row"><div class="col-12">
            <label for="language">'.__('Languages').'</label>
            '.$this->form->input_select(
                'language',
                '',
                '',
                'language',
                'class="js-choiceSelect"',
                '',
                isset($postMeta->language) ? maybe_unserialize($postMeta->language) : '',
                $languagesOpts,
                true
            ).'
        </div></div>';

        $siteversOpts = [];
        if(!empty($this->termsFields['sitever'])){
            foreach ($this->termsFields['sitever'] as $term) {
                $siteversOpts[$term->term_id] = $term->name;
            }
        }

        $html .= '<div class="row"><div class="col-12">
            <label for="sitever">'.__('Site versions').'</label>
            '.$this->form->input_select(
                'sitever',
                '',
                '',
                'sitever',
                'class="js-choiceSelect"',
                '',
                isset($postMeta->sitever) ? maybe_unserialize($postMeta->sitever) : '',
                $siteversOpts,
                true
            ).'
        </div></div>';

        $paymentsystemsOpts = [];
        if(!empty($this->termsFields['paymentsystem'])){
            foreach ($this->termsFields['paymentsystem'] as $term) {
                $paymentsystemsOpts[$term->term_id] = $term->name;
            }
        }

        $html .= '<div class="row"><div class="col-12">
            <label for="paymentsystems">'.__('Payment systems').'</label>
            '.$this->form->input_select(
                'paymentsystem',
                '',
                '',
                'paymentsystem',
                'class="js-choiceSelect"',
                '',
                isset($postMeta->paymentsystem) ? maybe_unserialize($postMeta->paymentsystem) : '',
                $paymentsystemsOpts,
                true
            ).'
        </div></div>';
        $html .= '</div>';

        echo $html;
    }

    public function onSavePost(int $post_id, WP_Post $post, bool $update)
    {
        if(!$update || $post->post_type !== 'review' || !isset($_GET['meta-box-loader'])) return false;

        if(wp_verify_nonce( $_POST['gamblingtest_metabox_one_nonce'], 'gamblingtest_metabox_one' )){
            $this->saveMeta($_POST, $post_id);
            OptimizedMetaSaver::updateFieldCache($post_id);
        }
    }

    private function saveMeta(array $postedData, int $post_id){
        foreach ($postedData as $key => $val){
            if(in_array($key, $this->metaFields)){
                switch ($key){
                    case 'language':
                    case 'sitever':
                    case 'paymentsystem':
                        $val = json_decode($val, false);

                        if (!empty($val) && is_array($val)){
                            $termids = [];
                            foreach ($val as $term_id){
                                $termids[] = $term_id;
                            }
                            wp_set_post_terms( $post_id, $termids, $key, true );
                        }
                        break;
                }
                update_post_meta($post_id, $key, $val);
            }
        }
    }

    private function generateTermsArr(){
        $resultArr = [];

        foreach ($this->taxsArr as $tax){
            $resultArr[$tax] = $this->getTermsList($tax);
        }

        return $resultArr;
    }

    private function getTermsList(string $taxName = ''){
        return get_terms([
            'taxonomy' => $taxName,
            'hide_empty' => false,
        ]);
    }
}
